-- Adminer 4.2.4 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `user_password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_role` enum('admin','kasir','unit') COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `UNIQ_8D93D64924A232CF` (`user_name`),
  UNIQUE KEY `UNIQ_8D93D649550872C` (`user_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

TRUNCATE `user`;
INSERT INTO `user` (`user_id`, `user_name`, `user_email`, `user_password`, `user_role`) VALUES
(1,	'Administrator',	'admin@mail.com',	'5f4dcc3b5aa765d61d8327deb882cf99',	'admin');

-- 2016-03-15 11:43:33
