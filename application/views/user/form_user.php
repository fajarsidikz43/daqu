<section class="content-header">
    <h1>
        User
        <small>User Form</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= base_url('admin') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?= base_url('user/') ?>">User</a></li>
        <li class="active">User Form</li>
    </ol>
</section>

<section class="content">
    <?php if ($this->session->flashdata()): ?>
        <div class="container-fluid">
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?= $this->session->flashdata('flash_data') ?>
            </div>
        </div>
    <?php endif ?>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-sm-12">
                        <form class="form-horizontal" action="<?= base_url('user/formUser') ?>" method="post">
                            <div class="form-group">
                                <label class="col-xs-3">Name</label>
                                <input type="text" name="name" value="<?= @$user->user_name ?>" class="form-control" placeholder="Your name" required="required"/>
                                <input type="hidden" name="user_id" value="<?= @$user->user_id ?>" />
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3">Email</label>
                                <input type="email" name="email" value="<?= @$user->user_email ?>" class="form-control" placeholder="Your Email" required="required"/>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3">password</label>
                                <input type="password" name="password" class="form-control" placeholder="Your Password" required="required"/>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3">Repeat password</label>
                                <input type="password" id="re_password" class="form-control" placeholder="Repeat Password" required="required"/>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3">Role</label>
                                <select name="user_role" required="required" class="form-control">
                                    <option value="">Pilih</option>
                                    <option <?php echo (@$user->user_role == 'admin') ? "selected='selected'" : "" ?> value="admin">Admin</option>
                                    <option <?php echo (@$user->user_role == 'kasir') ? "selected='selected'" : "" ?> value="user">Kasir</option>
                                    <option <?php echo (@$user->user_role == 'unit') ? "selected='selected'" : "" ?> value="user">Unit</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        $('form').submit(function(e) {
            var password = $('[name="password"]').val();
            var new_password_again = $('#re_password').val();

            if(password != new_password_again) {
                alert("Password dan Re-password Baru tidak sama!");
                e.preventDefault();
                return false;
            } else {
                return;
            }
        })
    });
</script>
