<section class="content-header">
    <h1>
        User
        <small>List of User</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= base_url('home') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User</li>
    </ol>
</section>

<section class="content">
    <?php if ($this->session->flashdata()): ?>
        <div class="container-fluid">
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?= $this->session->flashdata('flash_data') ?>
            </div>
        </div>
    <?php endif ?>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="<?= base_url('user/formUser') ?>" class="btn btn-primary btn-xs" ><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add User</a>
                </div>

                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover dataTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($users as $u): ?>
                            <tr>
                                <td></td>
                                <td><?= $u->user_name ?></td>
                                <td><?= $u->user_email ?></td>
                                <td><?= $u->user_role ?></td>
                                 <td>
                                    <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    Pilihan
                                    <span class="caret"></span>
                                    </button>
                                     <ul class="dropdown-menu" aria-labuser/elledby="dropdownMenu1">
                                    <li><a href="<?= base_url('user/formUser/'.$u->user_id) ?>" data-toggle="tooltip" data-placement="top" title="Update"><span class="fa fa-edit">Update</span></a></li>
                                    <li><a href="<?= base_url('user/deleteUser/'.$u->user_id) ?>" data-toggle="tooltip" data-placement="top" title="delete"><span class="fa fa-trash">Delete</span></a></li>
                                  </ul>
                                </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- ./panel -->
        </div>
    </div>
</section>
