<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Daqu</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?= base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/plugins/font-awesome/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/adminLTE/dist/css/AdminLTE.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/adminLTE/dist/css/skins/_all-skins.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/datatables/css/dataTables.bootstrap.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/datatables/css/dataTables.responsive.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/plugins/datepicker/jquery.datetimepicker.css'); ?>">

    <!-- JQUERY -->
    <script src="<?= base_url('assets/js/jquery-latest.min.js'); ?>"></script>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        <header class="main-header">
            <a href="index2.html" class="logo">
                <span class="logo-mini"><b>A</b>LT</span>
                <span class="logo-lg"><b>Daqu</b></span>
            </a>
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown messages-menu">
                            <a href="<?= base_url('login/logout') ?>">
                                <i class="fa fa-sign-out">Logout</i>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <aside class="main-sidebar">
            <section class="sidebar">
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?= base_url('assets/adminLTE/dist/img/admin.png') ?>" class="img-circle" alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p><?= $_SESSION['user_name'] ?>,</p>
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>

                <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </form>

                <ul class="sidebar-menu">
                    <li class="header">MAIN NAVIGATION</li>
                    <li>
                        <a href="<?= base_url('admin') ?>">
                            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= base_url('backend/user') ?>">
                            <i class="fa fa-user"></i> <span>User</span>
                        </a>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-bars"></i> <span>Category</span> <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="<?= base_url('backend/category') ?>">Category</a>
                            </li>
                            <li>
                                <a href="<?= base_url('backend/category/subcategory') ?>">Subcategory</a>
                            </li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-product-hunt"></i> <span>Product</span> <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="<?= base_url('backend/product/getProductType') ?>">Type</a>
                            </li>
                            <li>
                                <a href="<?= base_url('backend/product/getProductBrand') ?>">Brand</a>
                            </li>
                            <li>
                                <a href="<?= base_url('backend/product') ?>">Product</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?= base_url('backend/blog') ?>">
                            <i class="fa fa-rss"></i> <span>Blog</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= base_url('backend/order') ?>">
                            <i class="fa fa-shopping-cart"></i> <span>Orders</span>
                        </a>
                    </li>
                </ul>
            </section>
        </aside>

        <div class="content-wrapper">
            <?php $this->load->view($page) ?>
        </div>

        <footer class="main-footer">
            <strong>Copyright &copy; <?= date('Y') ?> <a href="#">Daqu</a>.</strong> All rights reserved.
        </footer>
    </div>

    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/sparkline/jquery.sparkline.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/knob/jquery.knob.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/fastclick/fastclick.min.js') ?>"></script>
    <script src="<?= base_url('assets/adminLTE/dist/js/app.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/datatables/js/jquery.dataTables.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/datatables/js/dataTables.bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/datatables/js/dataTables.responsive.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/datepicker/jquery.datetimepicker.full.min.js'); ?>"></script>
    <script src="<?= base_url('assets/plugins/input-mask/jquery.inputmask.js'); ?>"></script>
    <script src="<?= base_url('assets/plugins/input-mask/jquery.inputmask.extensions.js'); ?>"></script>
    <script src="<?= base_url('assets/plugins/ckeditor/ckeditor.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/ckeditor/adapters/jquery.js'); ?>"></script>
    <script src="<?= base_url('assets/plugins/ckeditor/styles.js'); ?>"></script>
    <script src="<?= base_url('assets/plugins/ckeditor/plugins/justify/plugin.js'); ?>"></script>

    <script>
        $(document).ready(function() {
            CKEDITOR.config.customConfig = "<?= base_url('assets/ckeditor/config.js'); ?>";

            $("[data-mask]").inputmask();

            // Tooltip
            $('[data-toggle="tooltip"]').tooltip();
            var t = $('.dataTable').DataTable( {
                responsive: true,
                "columnDefs": [ {
                    "searchable": false,
                    "orderable": false,
                    "targets": 0
                } ],
                "order": [[ 1, 'asc' ]]
            } );

            t.on( 'order.dt search.dt', function () {
                t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                } );
            } ).draw();

            // Datepicker
            $.datetimepicker.setLocale('id');
            $('.datepicker').datetimepicker({
                timepicker:false,
                format:'d/m/Y'
            });

            $('.datetimepicker').datetimepicker({
                format:'d/m/Y H:i:s'
            });
        });
    </script>
  </body>
</html>
