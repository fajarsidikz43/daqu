<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
    public function __construct() {
        parent::__construct();

        $this->load->model('user_model');
        if(!isset($_SESSION['user_id']) && $_SESSION['user_role'] != 'admin') {
			$this->session->set_flashdata('flash_data', 'You don\'t have access!');
			return redirect('login');
		}
    }

	public function index() {
	    $data['users'] = $this->user_model->getUsers();
		Template::backend('user/user', $data);
	}

	public function formUser($user_id = 0) {
	    if($_POST) {
	        $result = $this->user_model->formUser($_POST);
            if(isset($result['code'])) {
               $this->session->set_flashdata('flash_data',$result['message']);
            } else {
               $this->session->set_flashdata('flash_data',"User data already saved.");
            }
            redirect('user');
	    }

	    $data['user'] = $this->user_model->getUsers($user_id);
	    Template::backend('user/form_user', $data);
	}

	public function deleteUser($user_id) {
	    $this->user_model->deleteUser($user_id);
	    redirect('user');
	}
}
