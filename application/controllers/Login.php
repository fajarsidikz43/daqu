<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('login_model');
    }

	public function index()
	{
        if(isset($_SESSION['user_id']) && $_SESSION['user_role'] == 'admin') {
            return redirect('home');
        }
        if($_POST) {
            $result = $this->login_model->login($_POST);
            if(!empty($result)) {
                $this->session->set_userdata([
                    'user_id' => $result->user_id,
                    'user_email' => $result->user_email,
                    'user_name' => $result->user_name,
                    'user_role' => $result->user_role
                ]);
                redirect('home');
            } else {
                $this->session->set_flashdata('err', 'Wrong username or password.');
                redirect('login');
            }
        }
		$this->load->view('login');
	}
	
	public function logout() {
        session_destroy();
        redirect('login');
    }
}
