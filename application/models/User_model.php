<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Imron Rosdiana
 * @created_at 20 Juli 2015
 * @updated_at 20 Juli 2015
 */
class User_model extends CI_Model
{
    private $_mongoDb;

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function getUsers($user_id = 0) {
        $this->db->order_by('user_id', 'desc');
        if($user_id) {
            $query = $this->db->where('user_id', $user_id)->get('user')->row();
            return $query;
        }
        $query = $this->db->get('user')->result();
        return $query;
    }

    public function formUser($data) {
        $arr = [
            'user_name' => trim($data['name']),
            'user_email' => trim($data['email']),
            'user_password' => md5($data['password']),
            'user_role' => $data['user_role']
        ];
        if(!empty($data['user_id'])) {
            $this->db->where('user_id', $data['user_id']);
            $this->db->update('user', $arr);
        } else {
            $this->db->insert('user', $arr);
            if(!empty($this->db->error()['message'])) {
                return $this->db->error();
            }
        }
    }

    public function deleteUser($user_id) {
        $this->db->where('user_id', $user_id);
        $this->db->delete('user');
    }
}
