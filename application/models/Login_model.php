<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model
{
    private $_mongoDb;

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function login($params) {
        $email = trim($params['user_email']);
        $password = md5(trim($params['user_password']));

        $query = $this->db->get_where('user', array('user_email' => $email, 'user_password' => $password))->row();
        return $query;
    }

    public function __destruct() {
        $this->db->close();
    }
}
