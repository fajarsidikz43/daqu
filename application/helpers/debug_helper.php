<?php
/**
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Xlog Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Imron Rosdiana
 */

// ------------------------------------------------------------------------

if ( ! function_exists('xlog'))
{
	function xlog($data, $to_screen = true) {
         if ($to_screen) {
             echo '<pre class="-debug prettyprint">';
             print_r($data);
             echo '</pre>' . "\n";
         }
 	}
}

if ( ! function_exists('zlog'))
{
	function zlog($data, $to_screen = true) {
         if ($to_screen) {
             echo '<pre class="-debug prettyprint">';
             var_dump($data);
             echo '</pre>' . "\n";
         }
 	}
}
