<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* @author Imron Rosdiana
* @created at March 13, 2016
* @updated at March 13, 2016
*/

class Ongkir
{
	private $_CI;
	private $_config;
	private $_curl;

	private $_base_config = [
		CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => [
       		"key: 5d626e86e565911a59e9c8db9e3df557"
       	]
	];

	function __construct($debug = FALSE) {
		// get all ci default instance
		$this->_CI =& get_instance();
		$this->_config =& get_config();
		$this->_curl = curl_init();
	}

	public function response() {
		curl_setopt_array($this->_curl, $this->_base_config);
		$response = curl_exec($this->_curl);
        $err = curl_error($this->_curl);
        if ($err) {
           header('HTTP/1.1 500 Internal Server Error');
           return "cURL Error #:" . $err;
           exit;
        } else {
           	$k = json_decode($response, true);
           	header('Content-Type: application/json');
           	echo json_encode($k['rajaongkir']['results']);
        }
	}

	public function getCity() {
		$this->_base_config[CURLOPT_URL] = "http://rajaongkir.com/api/starter/city";
		$this->response();
	}
	
	public function jne_price($hometown, $destination) {
		$this->_base_config[CURLOPT_URL] = "http://rajaongkir.com/api/starter/cost";
		$this->_base_config[CURLOPT_CUSTOMREQUEST] = "POST";
		$this->_base_config[CURLOPT_POSTFIELDS] = "origin={$hometown}&destination={$destination}&weight=1000&courier=jne";
		$this->_base_config[CURLOPT_HTTPHEADER] = [
			"Content-Type: application/x-www-form-urlencoded",
    		"key: 5d626e86e565911a59e9c8db9e3df557"
		];

		$result = $this->response();
	}

	public function __destruct() {
		curl_close($this->_curl);
	}
	
}
