<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* @author Imron Rosdiana
* @created at April 26, 2015
* @updated at June 22, 2015
*/

class Template
{
	private static $_CI,
				   $_config;

	function __construct($debug = FALSE) {
		// get all ci default instance
		self::$_CI =& get_instance();
		self::$_config =& get_config();

		// Caching
        //self::$_CI->output->cache(5);
	}

	public static function __callStatic($name, $arguments) {
	    if($name == 'frontend') {
	        $layout = 'layout_frontend';
	    } elseif($name == 'backend') {
	        $layout = 'layout_backend';
	    } else {
	        header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
			echo "Function {$name} does not exists";
			exit(3);
	    }
	    
	    $count = count($arguments);
	    switch ($count) {
			// if load view without data array
			case '1':
				$data['page'] = $arguments[0];
				self::$_CI->load->view($layout, $data);
				break;

			// if load view with data array
			case '2':
                $data['page'] = $arguments[0];
                if(is_array($arguments[1])) {
                    $data = array_merge($data, $arguments[1]);
                };
				self::$_CI->load->view($layout, $data);
				break;

			default:
				throw new exception("Bad argument");
				break;
		}
    }
}
